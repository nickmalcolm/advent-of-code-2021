# Create a nested array of counts
# Each array item is a position, and
# within that is the count of 0s and 1s
inputs = File.readlines("test_input.txt", chomp: true)
length = inputs.first.length

counts = Array.new(length) { Array.new(2, 0) }

# Count the 0s and 1s by column in each input
inputs.each do |str|
  str.each_char.with_index do |bit, i|
    counts[i][bit.to_i] += 1
  end
end

# Find the most common bit of each position
most_common = counts.collect do |count|
  count[0] > count[1] ? 0 : 1
end.join("")


# The gamma rate is the number represented by that
# collection of most common bits.
# Ew eval. But hey, it works!
# https://docs.ruby-lang.org/en/2.0.0/syntax/literals_rdoc.html#label-Numbers
gamma_rate = eval("0b#{most_common}")

# The epsilon is the inverse.
# https://www.honeybadger.io/blog/bitwise-hacks-in-ruby/#rubys-bitwise-operators
epsilon_rate = eval("0b#{most_common} ^ 0b#{'1' * length}")

puts "ɣ: #{gamma_rate} ε: #{epsilon_rate}. ɣ*ε = #{gamma_rate * epsilon_rate}"