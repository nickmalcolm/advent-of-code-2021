# Before searching for either rating value, start with the full list of binary numbers from your diagnostic report and consider just the first bit of those numbers.
#   - Keep only numbers selected by the bit criteria for the type of rating value for which you are searching. Discard numbers which do not match the bit criteria.
#   - If you only have one number left, stop; this is the rating value for which you are searching.
#   - Otherwise, repeat the process, considering the next bit to the right.


# The bit criteria depends on which type of rating value you want to find:
#   - To find oxygen generator rating, determine the most common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 1 in the position being considered.
#   - To find CO2 scrubber rating, determine the least common value (0 or 1) in the current bit position, and keep only numbers with that bit in that position. If 0 and 1 are equally common, keep values with a 0 in the position being considered.


# Counts the 0s and 1s for a given position in each number
def count_bits_at_pos(numbers, i)
  # puts "Counting most common bit at #{i}"
  counts = [0,0]

  numbers.each do |number|
    counts[number.split('')[i].to_i] += 1
  end

  counts
end

def answer_for_criteria(bit_criteria)

  valid_numbers = File.readlines("input.txt", chomp: true).to_a

  valid_numbers.first.length.times do |i|

    counts = count_bits_at_pos(valid_numbers, i)

    if counts[0] != counts[1]

      # Reject the numbers where the ith position is NOT the most
      # common one from the ith position
      valid_numbers.delete_if do |str|

        bit = if bit_criteria.eql?(1)
          (counts[0] > counts[1] ? 0 : 1)
        else
          (counts[0] < counts[1] ? 0 : 1)
        end

        str.split('')[i].to_i != bit
      end

    else
      # Reject whichever does not have the current bit_criteria
      # at the ith position
      valid_numbers.delete_if do |str|
        str.split('')[i].to_i != bit_criteria
      end

    end

    break if valid_numbers.length == 1

  end

  return valid_numbers.first
end


oxygen = answer_for_criteria(1)
c02 = answer_for_criteria(0)

puts "#{oxygen}: " + eval("0b#{oxygen}").to_s
puts "#{c02}: " + eval("0b#{c02}").to_s
puts eval("0b#{oxygen}") * eval("0b#{c02}")







