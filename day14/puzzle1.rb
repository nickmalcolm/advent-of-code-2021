# a polymer template and a list of pair insertion rules


# A rule like AB -> C means that when elements A and B are
# immediately adjacent, element C should be inserted between them

def transform(template, pair_insertion_rules)
  pairs = (template.length - 1).times.collect {|i| "#{template[i]}#{template[i+1]}"}

  pairs.collect.with_index do |pair, i|
    if pair_insertion_rules.has_key?(pair)
      result = ''
      # Only add the first half the first time.
      # Each subsequent time, since pairs overlap,
      #  it's added by pair[1]
      if i.eql?(0)
        result += pair[0]
      end
      result += pair_insertion_rules[pair] + pair[1]
      # puts "Pair found! Changing #{pair} to #{result}"
      result
    end
  end.join('')
end

def calculate_score(template)
  counts = template.split('').each_with_object(Hash.new(0)) { |char,h| h[char] += 1 }
  max = counts.max_by {|k,v| v}[1]
  min = counts.min_by {|k,v| v}[1]
  puts "#{max} - #{min} = #{max - min}"
  max - min
end


# template = "NNCB"

# pair_insertion_rules = {
#   "CH" => "B",
#   "HH" => "N",
#   "CB" => "H",
#   "NH" => "C",
#   "HB" => "C",
#   "HC" => "B",
#   "HN" => "C",
#   "NN" => "C",
#   "BH" => "H",
#   "NC" => "B",
#   "NB" => "B",
#   "BN" => "B",
#   "BB" => "N",
#   "BC" => "B",
#   "CC" => "N",
#   "CN" => "C"
# }


lines = File.readlines('input.txt', chomp: true)
template = lines.shift
lines.shift # the new line
pair_insertion_rules = Hash.new
lines.each do |str|
  key, val = str.split(" -> ")
  pair_insertion_rules[key] = val
end

40.times do 
  template = transform(template, pair_insertion_rules)
end

puts template.length
puts calculate_score(template)