require 'pry'
# require 'pry-byebug'

require 'weighted_graph'

# Create a graph where a character is a vertex
# and the edges link pairs of characters, with
# edge weight indicating the number of times that
# edge occurs.

def add_edge(graph, src, dest, weight)
  if graph.contains_edge?(src, dest)
    weight += graph.get_edge_weight(src, dest)
  end
  # puts "Adding edge #{src}, #{dest}, #{weight}"
  graph.add_edge(src, dest, weight)
end

def remove_edge(graph, src, dest, weight)
  if graph.contains_edge?(src, dest)
    new_weight = graph.get_edge_weight(src, dest) - weight
    if new_weight > 0
      # puts "Reducing edge to (#{src},#{dest},#{weight -1})"
      graph.add_edge(src, dest, new_weight)
    else
      # puts "Removing edge entirely"
      graph.remove_edge(src, dest)
    end
  end
end

# Change A -> B to A -> X -> B, times number of times
def insert_node(graph, src, dest, new_node, times)
  # puts "Changing #{src} -> #{dest} to #{src} -> #{new_node} and #{new_node} -> #{dest}"
  # First, subtract a weight from A -> B (or remove it if it's the last one)
  remove_edge(graph, src, dest, times)
  # Second, add a weight from A -> X
  add_edge(graph, src, new_node, times)
  # And finally add a weight from X -> B
  add_edge(graph, new_node, dest, times)
end

def deep_copy(o)
  Marshal.load(Marshal.dump(o))
end

def transform(graph, pair_insertion_rules)
  original_edges = deep_copy(graph.instance_variable_get(:@edges))
  original_edges.each do |src, dests|
    dests.each do |dest, weight|
      pair = "#{src}#{dest}"
      # puts "Looking at #{pair}"
      if pair_insertion_rules.has_key?(pair)
        new_node = pair_insertion_rules[pair]
        # We have `weight` number of transformations to do
        # We need to remove src --> dest WEIGHT number of times
        # We need to add src -> new_node WEIGHT number of times
        # We need to add new_node -> dest WEIGHT number of times
        insert_node(graph, src, dest, new_node, weight)
      end
    end
  end
  graph
end

def build_initial_graph(template, pair_insertion_rules)
  graph = WeightedGraph::Graph.new

  pairs = (template.length - 1).times.collect {|i| "#{template[i]}#{template[i+1]}"}
  pairs.each do |pair|
    add_edge(graph, pair[0], pair[1], 1)
  end

  graph
end

def count_nodes(graph)
  counts = Hash.new(0)
  edges = graph.instance_variable_get(:@edges)

  # For the initial node
  counts[edges.keys.first] += 1

  graph.instance_variable_get(:@edges).each do |src, dests|
    dests.each do |dest, weight|
      counts[dest] += weight
    end
  end

  counts
end

def length(graph)
  length = 0
  edges = graph.instance_variable_get(:@edges)

  # For the initial node
  length += 1

  graph.instance_variable_get(:@edges).each do |src, dests|
    dests.each do |dest, weight|
      length += weight
    end
  end

  length
end

def calculate_score(counts)
  max = counts.max_by {|k,v| v}[1]
  min = counts.min_by {|k,v| v}[1]
  puts "#{max} - #{min} = #{max - min}"
  max - min
end

# template = "NNCB"

# pair_insertion_rules = {
#   "CH" => "B",
#   "HH" => "N",
#   "CB" => "H",
#   "NH" => "C",
#   "HB" => "C",
#   "HC" => "B",
#   "HN" => "C",
#   "NN" => "C",
#   "BH" => "H",
#   "NC" => "B",
#   "NB" => "B",
#   "BN" => "B",
#   "BB" => "N",
#   "BC" => "B",
#   "CC" => "N",
#   "CN" => "C"
# }

lines = File.readlines('input.txt', chomp: true)
template = lines.shift
lines.shift # the new line
pair_insertion_rules = Hash.new
lines.each do |str|
  key, val = str.split(" -> ")
  pair_insertion_rules[key] = val
end

graph = build_initial_graph(template, pair_insertion_rules)
count_nodes(graph)

40.times do |i|
  puts ""
  puts "--------------"
  puts "  Step #{i+1}"
  puts "--------------"

  transform(graph, pair_insertion_rules)
  p graph
  puts "Length: #{length(graph)}"
  puts counts = count_nodes(graph)
  puts "Score: #{calculate_score(counts)}"
end

#pry.debugger



# lines = File.readlines('input.txt', chomp: true)
# template = lines.shift
# lines.shift # the new line
# pair_insertion_rules = Hash.new
# lines.each do |str|
#   key, val = str.split(" -> ")
#   pair_insertion_rules[key] = val
# end

# 10.times do 
#   template = transform(template, pair_insertion_rules)
#   puts template
#   puts calculate_score(template)
# end
 