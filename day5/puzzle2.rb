class Point
  attr_accessor :x
  attr_accessor :y

  def initialize(x, y)
    self.x = x
    self.y = y
  end
end
class Line
  attr_accessor :p1
  attr_accessor :p2

  def initialize(p1, p2)
    self.p1 = p1
    self.p2 = p2
  end

  def diagonal?
    p1.x != p2.x && p1.y != p2.y
  end

  # Diagonals are always 45°
  def diagonal_points
    points = Array.new
    x = x_range
    y = y_range
    x.collect {|n| [n, y.shift]}
  end


  def x_range
    if p1.x < p2.x
      (p1.x..p2.x).to_a.reverse
    else
      (p2.x..p1.x).to_a
    end
  end

  def y_range
    if p1.y < p2.y
      (p1.y..p2.y).to_a.reverse
    else
      (p2.y..p1.y).to_a
    end
  end

end
class Graph
  attr_accessor :graph
  attr_accessor :lines
  attr_accessor :w
  attr_accessor :h

  def initialize(w: 10, h: 10)
    self.w = w
    self.h = h
    self.graph = Array.new(w) { Array.new(h, 0) }
  end

  def draw
    h.times do |y|
      w.times do |x|
        count = self.graph[x][y]
        print count.eql?(0) ? "." : count
      end
      print "\n"
    end
  end

  def add_line(line)
    self.lines |= line
    update_graph_for(line)
  end

  def update_graph_for(line)
    points_x = line.x_range
    points_y = line.y_range

    if line.diagonal?
      line.diagonal_points.each do |point|
        self.graph[point[0]][point[1]] += 1
      end
    elsif points_x.length == 1
      x = points_x.first
      # Horizontal line
      points_y.each {|py| self.graph[x][py] += 1}
    elsif points_y.length == 1
      y = points_y.first
      # Vertical line
      points_x.each {|px| self.graph[px][y] += 1}
    else
      p line
      raise "Uh oh"
    end
  end

  def points_with_overlap
    overlap = 0
    h.times do |y|
      w.times do |x|
        count = self.graph[x][y]
        overlap +=1 if count > 1
      end
    end
    overlap
  end

end

INPUT_REGEX = /(\d+),(\d+) -> (\d+),(\d+)/
lines = Array.new
max_x = 0
max_y = 0

# Simple test
# g = Graph.new(w: 10, h: 10)
# g.add_line(Line.new(Point.new(1,2),Point.new(4,5)))
# g.add_line(Line.new(Point.new(0,0),Point.new(8,8)))
# g.add_line(Line.new(Point.new(8,0),Point.new(0,8)))

File.foreach("input.txt") do |line|
  points = INPUT_REGEX.match line

  max_x = [max_x, points[1].to_i, points[3].to_i].max
  max_y = [max_y, points[2].to_i, points[4].to_i].max

  lines << Line.new(
    Point.new(points[1].to_i, points[2].to_i),
    Point.new(points[3].to_i, points[4].to_i)
  )
end


puts "Creating #{max_x+1} by #{max_y+1} graph"
g = Graph.new(w: max_x+1, h: max_y+1)
lines.each {|l| g.add_line(l) }


# g.draw

puts g.points_with_overlap