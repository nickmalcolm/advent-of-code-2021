require 'pry'
require 'pry-byebug'

require 'rgl/adjacency'
require 'rgl/implicit'
require 'rgl/dot'


# Find all possible paths
# All paths you find should visit small caves at most once, and 
# can visit big caves any number of times.


class PathFinder
  attr_accessor :graph
  # Keeps track of valid paths
  attr_accessor :valid_paths
  # A temporary but recursive-function-accessible attr
  # for currently valid (but still being worked out) paths
  attr_accessor :current_path

  def initialize(edges:)
    self.graph = RGL::DirectedAdjacencyGraph.new
    edges.each do |u, v|
      graph.add_edge u, v
      graph.add_edge v, u
    end
    self.valid_paths = Array.new
    self.current_path = Array.new
  end

  def find_all_paths(source, target)
    # Start at the source
    # binding.pry
    graph.edges_filtered_by {|u, v| u.eql?(source) }.each_edge do |u, v|

      # Don't go back to the start
      if v.eql?("start")
        next
      end

      # puts "source: #{source}, target: #{target}, u: #{u}, v: #{v}"
      # puts " ↳ current_path is #{self.current_path.join(',')}"
      # binding.pry
      if v.eql?(target)
        # We've found a valid path, so add that, reset CURRENT_PATH,
        # and move on
        # puts "#{self.current_path.join(',')} is a valid path!"
        # binding.pry
        self.valid_paths << self.current_path.dup
        # self.current_path.pop
      else
        # Use recursion to build a path of potential paths

        # Don't visit a lowercase "cave" more than once
        if v != v.upcase && self.current_path.include?(v)
          # This is a lowercase node we've already visited
          # No-op / don't check v
        else
          self.current_path << v
          find_all_paths(v, target)
          # puts "popping!"
          self.current_path.pop
        end

      end
    end
  end

  def make_img
    graph.write_to_graphic_file('jpg')
  end
end


# edges = [
#   "start-A",
#   "start-b",
#   "A-c",
#   "A-b",
#   "b-d",
#   "A-end",
#   "b-end"
# ].collect {|s| s.split("-") }

# edges = [
#   "dc-end",
#   "HN-start",
#   "start-kj",
#   "dc-start",
#   "dc-HN",
#   "LN-dc",
#   "HN-end",
#   "kj-sa",
#   "kj-HN",
#   "kj-dc"
# ].collect {|s| s.split("-") }
# There are 19 valid paths

# edges = [
#   "fs-end",
#   "he-DX",
#   "fs-he",
#   "start-DX",
#   "pj-DX",
#   "end-zg",
#   "zg-sl",
#   "zg-pj",
#   "pj-he",
#   "RW-he",
#   "fs-DX",
#   "pj-RW",
#   "zg-RW",
#   "start-pj",
#   "he-WI",
#   "zg-he",
#   "pj-fs",
#   "start-RW"
# ].collect {|s| s.split("-") }
# There are 226 valid paths

edges = [
  "xx-end",
  "EG-xx",
  "iy-FP",
  "iy-qc",
  "AB-end",
  "yi-KG",
  "KG-xx",
  "start-LS",
  "qe-FP",
  "qc-AB",
  "yi-start",
  "AB-iy",
  "FP-start",
  "iy-LS",
  "yi-LS",
  "xx-AB",
  "end-KG",
  "iy-KG",
  "qc-KG",
  "FP-xx",
  "LS-qc",
  "FP-yi"
].collect {|s| s.split("-") }

pf = PathFinder.new(edges: edges)
pf.find_all_paths("start", "end")
# pf.valid_paths.each do |path|
#   puts "start,#{path.join(',')},end"
# end

puts "There are #{pf.valid_paths.length} valid paths"

