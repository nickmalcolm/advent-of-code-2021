# [horizonta,depth]
POSITION = [0,0]

def move_by(instruction)
  direction, amount = instruction.split(" ")
  amount = amount.to_i

  case direction
  when "forward"
    POSITION[0] += amount
  when "down"
    POSITION[1] += amount
  when "up"
    POSITION[1] -= amount
  end
end

File.foreach("input.txt").with_index do |instruction|
   move_by(instruction)
end

puts "Horizontal: #{POSITION[0]}. Depth: #{POSITION[1]}"
puts "Answer: #{POSITION[0] * POSITION[1]}"
