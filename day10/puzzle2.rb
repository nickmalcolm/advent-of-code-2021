# The navigation subsystem syntax is made of several lines containing chunks. 
# There are one or more chunks on each line, and chunks contain zero or more other chunks. 
# Adjacent chunks are not separated by any delimiter; if one chunk stops, 
# the next chunk (if any) can immediately start. Every chunk must open and close with one 
# of four legal pairs of matching characters:
#  - If a chunk opens with (, it must close with ).
#  - If a chunk opens with [, it must close with ].
#  - If a chunk opens with {, it must close with }.
#  - If a chunk opens with <, it must close with >.
#
# So, () is a legal chunk that contains no other chunks, as is []. 
# More complex but valid chunks include 
#   ([]), {()()()}, <([{}])>, [<>({}){}[([])<>]], and even (((((((((()))))))))).

# Some lines are incomplete, but others are corrupted. Find and discard the corrupted lines first.

# A corrupted line is one where a chunk closes with the wrong character - that is, 
# where the characters it opens and closes with do not form one of the four legal pairs listed above.

# Examples of corrupted chunks include (], {()()()>, (((()))}, and <([]){()}[{}]). 
# Such a chunk can appear anywhere within a line, and its presence causes the whole
# line to be considered corrupted.

# Stop at the first incorrect closing character on each corrupted line.



OPENING_CHARS = ['(','[','{','<']
CLOSING_CHARS = [')',']','}','>']



# Returns a score for incomplete lines
def parse_line(line)
  unclosed = []
  line.split('').each.with_index do |c, i|
    if OPENING_CHARS.include?(c)
      unclosed << c
    else
      # Check to see if we're closing the most recently opened tag
      last_opened = unclosed.last
      expected_closer = CLOSING_CHARS[OPENING_CHARS.index(last_opened)]
      if expected_closer.eql?(c)
        # This is a valid closer.
        # Remove the opened character
        unclosed.pop
      else
        # Skip corrupt lines
        return
      end
    end
  end

  if unclosed.any?
    # We need to finish this line off
    # and score as we go
    score = 0
    unclosed.reverse.each do |c|
      score *= 5
      case c
      when '('
        score += 1
      when '['
        score += 2
      when '{'
        score += 3
      when '<'
        score += 4
      end
    end

    # puts "#{line} has #{unclosed} unclosed - score is #{score}"
    return score
  else
    # No op
  end
end


scores = File.readlines("input.txt", chomp: true).collect do |line|
  parse_line(line)
end.compact

# puts scores
puts scores.sort[scores.length / 2]