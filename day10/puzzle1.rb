# The navigation subsystem syntax is made of several lines containing chunks. 
# There are one or more chunks on each line, and chunks contain zero or more other chunks. 
# Adjacent chunks are not separated by any delimiter; if one chunk stops, 
# the next chunk (if any) can immediately start. Every chunk must open and close with one 
# of four legal pairs of matching characters:
#  - If a chunk opens with (, it must close with ).
#  - If a chunk opens with [, it must close with ].
#  - If a chunk opens with {, it must close with }.
#  - If a chunk opens with <, it must close with >.
#
# So, () is a legal chunk that contains no other chunks, as is []. 
# More complex but valid chunks include 
#   ([]), {()()()}, <([{}])>, [<>({}){}[([])<>]], and even (((((((((()))))))))).

# Some lines are incomplete, but others are corrupted. Find and discard the corrupted lines first.

# A corrupted line is one where a chunk closes with the wrong character - that is, 
# where the characters it opens and closes with do not form one of the four legal pairs listed above.

# Examples of corrupted chunks include (], {()()()>, (((()))}, and <([]){()}[{}]). 
# Such a chunk can appear anywhere within a line, and its presence causes the whole
# line to be considered corrupted.

# Stop at the first incorrect closing character on each corrupted line.

class CorruptLine
  attr_accessor :error_at
  attr_accessor :error
  attr_accessor :illegal_char
  def initialize(i, illegal_char, err)
    self.error_at = i
    self.illegal_char = illegal_char
    self.error = err
  end
  def to_s
    "#{error} - #{score}"
  end
  def score
    case illegal_char
    when ')'
      3
    when ']'
      57
    when '}'
      1197
    when '>'
      25137
    end
  end
end
class ValidLine
  def to_s
    "Valid"
  end
end
class IncompleteLine
  def to_s
    "Incomplete"
  end
end


OPENING_CHARS = ['(','[','{','<']
CLOSING_CHARS = [')',']','}','>']



# Returns an Object either CorruptLine, ValidLine, or IncompleteLine
def parse_line(line)
  unclosed = []
  line.split('').each.with_index do |c, i|
    if OPENING_CHARS.include?(c)
      unclosed << c
    else
      # Check to see if we're closing the most recently opened tag
      last_opened = unclosed.last
      expected_closer = CLOSING_CHARS[OPENING_CHARS.index(last_opened)]
      if expected_closer.eql?(c)
        # This is a valid closer.
        # Remove the opened character
        unclosed.pop
      else
        return CorruptLine.new(i, c, "Expected #{expected_closer} but got #{c} instead")
      end
    end
  end

  if unclosed.any?
    return IncompleteLine.new
  else
    return ValidLine.new
  end
end

corruptions = []
File.readlines("input.txt", chomp: true).each do |line|
  resp = parse_line(line)
  # puts "#{line}: #{resp}"
  if resp.is_a? CorruptLine
    corruptions << resp
  end
end

puts corruptions.length
puts corruptions.collect {|c| c.score }.sum