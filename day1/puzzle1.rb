# data = [199,200,208,210,200,207,240,269,260,263]

# count the number of times a depth measurement increases from the previous measurement. (There is no measurement before the first measurement.) In the example above, the changes are as follows:

# 199 (N/A - no previous measurement)
# 200 (increased)
# 208 (increased)
# 210 (increased)
# 200 (decreased)
# 207 (increased)
# 240 (increased)
# 269 (increased)
# 260 (decreased)
# 263 (increased)

# data = IO.readlines("test_input.txt").map(&:to_i)
data = IO.readlines("input.txt").map(&:to_i)


previous_measurement = data.shift

puts "#{previous_measurement} (N/A - no previous measurement)"

increases = 0

data.each do |measurement|
  if measurement > previous_measurement
    increases += 1
    puts "#{measurement} (increased)"
  else
    puts "#{measurement} (decreased)"
  end
  previous_measurement = measurement
end

puts "#{increases} increases"
