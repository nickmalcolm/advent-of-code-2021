# consider sums of a three-measurement sliding window.

# data = IO.readlines("test_input.txt").map(&:to_i)
data = IO.readlines("input.txt").map(&:to_i)

WINDOW = 3

summed = (data.length - WINDOW + 1).times.collect do |i|
  # Sum the three values in the window
  data[i..i+WINDOW - 1].sum
end

previous_measurement = summed.shift

puts "#{previous_measurement} (N/A - no previous sum)"

increases = 0

summed.each do |measurement|
  if measurement > previous_measurement
    increases += 1
    puts "#{measurement} (increased)"
  else
    puts "#{measurement} (decreased)"
  end
  previous_measurement = measurement
end

puts "#{increases} increases"