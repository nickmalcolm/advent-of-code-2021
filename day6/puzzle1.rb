class LanternFish
  attr_accessor :days_til_spawn

  def initialize(days_til_spawn: 9)
    self.days_til_spawn = days_til_spawn
  end

  # Returns its child if it spawns one
  def progress!
    if self.days_til_spawn.eql? 0
      self.days_til_spawn = 6
      return LanternFish.new
    else
      self.days_til_spawn -= 1
      return false
    end
  end

end

input = [2,4,1,5,1,3,1,1,5,2,2,5,4,2,1,2,5,3,2,4,1,3,5,3,1,3,1,3,5,4,1,1,1,1,5,1,2,5,5,5,2,3,4,1,1,1,2,1,4,1,3,2,1,4,3,1,4,1,5,4,5,1,4,1,2,2,3,1,1,1,2,5,1,1,1,2,1,1,2,2,1,4,3,3,1,1,1,2,1,2,5,4,1,4,3,1,5,5,1,3,1,5,1,5,2,4,5,1,2,1,1,5,4,1,1,4,5,3,1,4,5,1,3,2,2,1,1,1,4,5,2,2,5,1,4,5,2,1,1,5,3,1,1,1,3,1,2,3,3,1,4,3,1,2,3,1,4,2,1,2,5,4,2,5,4,1,1,2,1,2,4,3,3,1,1,5,1,1,1,1,1,3,1,4,1,4,1,2,3,5,1,2,5,4,5,4,1,3,1,4,3,1,2,2,2,1,5,1,1,1,3,2,1,3,5,2,1,1,4,4,3,5,3,5,1,4,3,1,3,5,1,3,4,1,2,5,2,1,5,4,3,4,1,3,3,5,1,1,3,5,3,3,4,3,5,5,1,4,1,1,3,5,5,1,5,4,4,1,3,1,1,1,1,3,2,1,2,3,1,5,1,1,1,4,3,1,1,1,1,1,1,1,1,1,2,1,1,2,5,3]

school = input.collect {|i| LanternFish.new(days_til_spawn: i)}

# puts "Initial state:   #{school.collect(&:days_til_spawn).join(',')}"

(1..256).each do |i|
  school.each do |fish|
    if spawned = fish.progress!
      school << spawned
    end
  end
  if i % 1 == 0
    puts "#{school.length} fish after #{i} days"
  end
  # puts "After #{i.to_s.rjust(2)} day(s): #{school.collect(&:days_til_spawn).join(',')}"
end

puts "#{school.length} fish"

