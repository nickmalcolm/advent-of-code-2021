# The submarine generates a heightmap of the floor of the nearby caves
# Smoke flows to the lowest point of the area it's in.

# Your first goal is to find the low points - the locations that are lower than
# any of its adjacent locations. 
# Most locations have four adjacent locations (up, down, left, and right); 
# locations on the edge or corner of the map have three or two adjacent locations, respectively.
# (Diagonal locations do not count as adjacent.)

require 'pry'

require 'rgl/adjacency'
require 'rgl/implicit'
require 'rgl/dot'
require 'rgl/base'

class Node
  attr_accessor :row
  attr_accessor :column
  attr_accessor :height
  attr_accessor :group_id

  def initialize(row:, column:, height:)
    self.row = row
    self.column = column
    self.height = height
    if row.nil? || column.nil? || height.nil?
      raise "Invalid: (#{row}, #{column}, #{height})"
    end
    if height.eql?(9)
      raise "Shouldn't add 9s!( #{row}, #{column})"
    end
  end

  # eql is used when adding vertices to the hash
  def ==(other)
    other.is_a?(Node) && self.row == other.row && self.column == other.column
    # if other.is_a?(Node) && self.row == other.row && self.column == other.column
    #   if self.height.eql?(other.height)
    #     return true
    #   else
    #     raise "Oh no! You're trying to add a new value to an existing Node (#{self.to_s} vs. #{other.to_s}"
    #   end
    # end
    # false
  end
  def eql?(other)
    self == other
  end
  def hash
    [self.row, self.column, self.height].hash
  end

  def to_s
    "(r#{row}, c#{column}, h#{height}, g#{group_id})"
  end
end


graph = RGL::AdjacencyGraph.new

heightmap = File.readlines("input.txt", chomp: true).collect {|line| line.split('').map(&:to_i) }

group_id = 0

heightmap.each_with_index do |row, i|
  row.each_with_index do |height, j|
    # puts "#{i}, #{j}: #{height}"

    # Ignore 9s
    next if height.eql?(9)

    # Look for valid adjacent nodes
    node_below = nil
    node_right  = nil

    # Down
    if i < heightmap.length - 1
      if heightmap[i+1][j] != 9
        node_below = Node.new(row: i+1, column: j, height: heightmap[i+1][j])
      end
    end
    # Right
    if j < row.length - 1
      if heightmap[i][j+1] != 9
        node_right = Node.new(row: i, column: j+1, height: heightmap[i][j+1])
      end
    end

    # Create an initial node (it might already exist)
    node = Node.new(row: i, column: j, height: height)

    # We want to keep track of groups of connected nodes as we
    # build the graph
    this_group_id = nil
    
    # If the node is already in the graph, it'll already have a group ID
    [node, node_below, node_right].each do |n|
      if n && graph.has_vertex?(n)
        # Get the existing group id
        x = graph.instance_variable_get(:@vertices_dict).keys.select {|k| k.eql?(n)}.first.group_id
        
        # Sometimes we run into a scenario where two groups merge
        # while building the graph
        if !this_group_id.nil? && !x.nil? && !this_group_id.eql?(x)
          # binding.pry
          # We'll update everything to use `this_group_id`
          # puts "Resolving merge conflict between group #{x} and #{this_group_id} (choosing the latter)"
          graph.each_edge do |u, v|
            if u.group_id.eql?(x) || v.group_id.eql?(x)
              u.group_id = this_group_id
              v.group_id = this_group_id
              graph.add_edge(u, v)
            end
          end
          x = this_group_id
        end
        this_group_id ||= x
      end
    end

    if this_group_id.nil?
      # Time for a new group
      group_id += 1
      this_group_id = group_id
    end

    # Add the group id to the node objects
    node.group_id = this_group_id
    
    # Add the node and two neighbours.
    # Since Node reimplements hash/eql to ignore group id,
    # we won't end up with duplicate vertices

    if node_below
      node_below.group_id = this_group_id
      graph.add_edge(node, node_below)
    end
    
    if node_right
      node_right.group_id = this_group_id
      graph.add_edge(node, node_right)
    end

  end
end

puts "Created graph"

# For debugging
# If this is uncommented you need to
# brew install graphviz
# graph.write_to_graphic_file('jpg')

# Now iterate over the vertices and count by group IDs
# binding.pry
basins_w_size = graph.vertices.each_with_object(Hash.new(0)) { |node, h| h[node.group_id] += 1 }
largest_three = basins_w_size.to_a.sort_by {|a| a[1] }.last(3)

puts answer = largest_three.collect {|a| a[1]}.inject(:*)



