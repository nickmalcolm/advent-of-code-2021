# The submarine generates a heightmap of the floor of the nearby caves
# Smoke flows to the lowest point of the area it's in.

# Your first goal is to find the low points - the locations that are lower than
# any of its adjacent locations. 
# Most locations have four adjacent locations (up, down, left, and right); 
# locations on the edge or corner of the map have three or two adjacent locations, respectively.
# (Diagonal locations do not count as adjacent.)


heightmap = File.readlines("input.txt", chomp: true).collect {|line| line.split('').map(&:to_i) }

low_points = []

heightmap.each_with_index do |row, i|
  row.each_with_index do |height, j|

    # 9 is always the high point and can't be a low point
    next if height.eql?(9)
    # 0 is always a low point
    if height.eql?(0)
      low_points << height
      next
    end

    # It's not 9 or 0 so let's look around
    adjacents = []
    # Up
    adjacents << heightmap[i-1][j] unless i == 0
    # Down
    adjacents << heightmap[i+1][j] unless i == (heightmap.length - 1)
    # Left
    adjacents << heightmap[i][j-1] unless j == 0
    # Right
    adjacents << heightmap[i][j+1] unless j == (row.length - 1)

    # If all adjacents are bigger, it's a lowest point
    lowest_point = adjacents.all? { |a| height < a }
    if lowest_point 
      low_points << height
    end
  end
end

risk = low_points.collect {|n| n + 1}.sum
puts "The risk of the heightmap is #{risk}"


# The risk level of a low point is 1 plus its height.

# What is the sum of the risk levels of all low points on your heightmap?