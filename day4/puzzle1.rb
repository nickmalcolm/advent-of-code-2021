class BingoNumber
  attr_accessor :number
  attr_accessor :called

  def initialize(number)
    self.number = number.to_i
    self.called = false
  end

  def to_s
    number.to_s.rjust(3) + (called ? '*' : ' ')
  end
end

class BingoBoard
  attr_accessor :set
  attr_accessor :board
  attr_accessor :winner

  # Set is the parent BingoBoardSet
  # Lines should be five lines of five numbers
  def initialize(set, lines)
    self.set = set
    self.board = lines.collect do |line|
      line.split(' ').collect { |n| set.bingo_number(n.to_i) }
    end
  end

  def to_s
    str = board.collect do |line|
      line.collect do |number|
        number.to_s
      end.join(" ")
    end.join("\n")

    if winner
      str = "!!! WINNER !!!\n#{str}\n!!! WINNER !!!\n"
    end

    str
  end

  def winner?
    self.winner = winner_x? || winner_y?
  end

  def sum_uncalled
    board.sum do |line|
      line.sum do |n|
        n.called ? 0 : n.number
      end
    end
  end

  private

  def winner_x?
    board.any? do |line|
      line.all? {|n| n.called }
    end
  end

  def winner_y?
    5.times.any? do |i|
      board.all? do |line|
        line[i].called
      end
    end
  end

end

class BingoBoardSet
  attr_accessor :bingo_numbers
  attr_accessor :bingo_boards
  attr_accessor :winners

  def initialize
    self.bingo_numbers = Array.new(100)
    self.bingo_boards = Array.new
    self.winners = Array.new
  end

  def bingo_number(n)
    bingo_numbers[n] ||= BingoNumber.new(n)
  end

  def add_board(lines)
    bingo_boards << BingoBoard.new(self, lines)
  end

  def call(n)
    bingo_numbers[n].called = true
  end

  def check_for_winners
    # Evaluate all boards
    self.winners = bingo_boards.each.select do |board|
      board if board.winner?
    end
    # And return true if any one wins
    winners.any?
  end

  def to_s
    bingo_boards.collect(&:to_s).join("\n\n")
  end
end

input = IO.readlines('input.txt')
bingo_calls = input.shift.split(',').map(&:to_i)

board_set = BingoBoardSet.new

input.each_slice(6).collect do |slice|
  slice.shift # remove the newline
  board_set.add_board(slice)
end

while bingo_calls.length > 0
  call = bingo_calls.shift
  puts "Called #{call}"
  board_set.call(call)
  if board_set.check_for_winners
    puts "DING DING DING"
    puts board_set.to_s
    uncalled_sum = board_set.winners.first.sum_uncalled
    puts "#{uncalled_sum} * #{call} = #{uncalled_sum * call}"
    break
  end
end