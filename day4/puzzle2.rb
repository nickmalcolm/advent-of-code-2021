class BingoNumber
  attr_accessor :number
  attr_accessor :called

  def initialize(number)
    self.number = number.to_i
    self.called = false
  end

  def to_s
    number.to_s.rjust(3) + (called ? '*' : ' ')
  end
end

class BingoBoard
  attr_accessor :set
  attr_accessor :board
  attr_accessor :winner
  attr_accessor :sum_when_won

  # Set is the parent BingoBoardSet
  # Lines should be five lines of five numbers
  def initialize(set, lines)
    self.set = set
    self.board = lines.collect do |line|
      line.split(' ').collect { |n| set.bingo_number(n.to_i) }
    end
  end

  def to_s
    str = board.collect do |line|
      line.collect do |number|
        number.to_s
      end.join(" ")
    end.join("\n")

    if winner
      str = "!!! WINNER !!!\n#{str}\n!!! WINNER !!!\n"
    end

    str
  end

  def check_winner?
    # Don't check yourself again once you've won
    # If you do win, capture sum_uncalled
    unless self.winner
      if self.winner = winner_x? || winner_y?
        self.sum_when_won = sum_uncalled
      end
    end
    self.winner
  end

  def sum_uncalled
    board.sum do |line|
      line.sum do |n|
        n.called ? 0 : n.number
      end
    end
  end

  private

  def winner_x?
    board.any? do |line|
      line.all? {|n| n.called }
    end
  end

  def winner_y?
    5.times.any? do |i|
      board.all? do |line|
        line[i].called
      end
    end
  end

end

class BingoBoardSet
  attr_accessor :bingo_numbers
  attr_accessor :bingo_boards
  attr_accessor :winners
  attr_accessor :winning_calls

  def initialize
    self.bingo_numbers = Array.new(100)
    self.bingo_boards = Array.new
    self.winners = Array.new
    self.winning_calls = Array.new
  end

  def bingo_number(n)
    bingo_numbers[n] ||= BingoNumber.new(n)
  end

  def add_board(lines)
    bingo_boards << BingoBoard.new(self, lines)
  end

  # Call the bingo number, and add the number
  # to winning calls if this created a new
  # winner
  def call(n)
    prior_winner_count = self.winners.length
    bingo_numbers[n].called = true
    check_for_winners
    winner_count = self.winners.length
    if winner_count > prior_winner_count
      self.winning_calls << n
    end

  end

  def check_for_winners
    # Evaluate all boards
    to_check = bingo_boards.reject(&:winner)
    to_check.each do |board|
      self.winners << board if board.check_winner?
    end
    # And return true if any one wins
    self.winners.any?
  end

  def to_s
    bingo_boards.collect(&:to_s).join("\n\n")
  end
end

input = IO.readlines('input.txt')
bingo_calls = input.shift.split(',').map(&:to_i)

board_set = BingoBoardSet.new

input.each_slice(6).collect do |slice|
  slice.shift # remove the newline
  board_set.add_board(slice)
end

while bingo_calls.length > 0
  call = bingo_calls.shift
  board_set.call(call)
end

# Now get the last board to win
last_winner = board_set.winners.pop
last_winning_call = board_set.winning_calls.pop

uncalled_sum = last_winner.sum_when_won
puts "#{uncalled_sum} * #{last_winning_call} = #{uncalled_sum * last_winning_call}"