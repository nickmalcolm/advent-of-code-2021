require 'pry'

# Each octopus has an energy level between 0 and 9
# Each octopus slowly gains energy over time and flashes brightly for a moment when its energy is full. 

class Octopus
  attr_accessor :energy
  attr_accessor :flashed
  def initialize(energy:)
    self.energy = energy
    self.flashed = false
  end
  def step!
    self.energy += 1
  end
  def flash!
    self.flashed = true
  end

  def flashable?
    energy > 9 && !self.flashed
  end

  def reset!
    self.energy = 0
    self.flashed = false
  end
end


# During a single step, the following occurs:
# 
#   - First, the energy level of each octopus increases by 1.
#   - Then, any octopus with an energy level greater than 9 flashes. 
#     + This increases the energy level of all adjacent octopuses by 1, including octopuses that are diagonally adjacent. 
#     + If this causes an octopus to have an energy level greater than 9, it also flashes. 
#     + This process continues as long as new octopuses keep having their energy level increased beyond 9. 
#     + (An octopus can only flash at most once per step.)
#   - Finally, any octopus that flashed during this step has its energy level set to 0, as it used all of its energy to flash.

# When an octopus flashes, step! all its
# neighbours
def step_adjacent(octopi, row, col)
  # puts "step_adjacent(o, #{row}, #{col}"
  # Up
  # binding.pry
  [
    [row-1, col-1], # NW
    [row-1, col],   # N
    [row-1, col+1], # NE
    [row,   col+1], # E
    [row+1, col+1], # SE
    [row+1, col],   # S
    [row+1, col-1], # SW
    [row,   col-1], # W
  ].each do |row, col|

    next if row < 0 || row > octopi.length - 1
    next if col < 0 || col > octopi[row].length - 1

    octopus = octopi[row][col]
    octopus.step!
    # puts "  Stepped #{row},#{col} to #{octopus.energy}"
    if octopus.flashable?
      # puts "  #{row},#{col} is flashing too!"
      octopus.flash!
      step_adjacent(octopi, row, col)
    end
  end
end

def step(octopi)
  # First update all octopi
  octopi.each do |octopi_row|
    octopi_row.each do |octopus|
      octopus.step!
    end
  end
  # puts "After part 1"
  # puts octopi.map {|r| r.collect(&:energy).join('') }.join("\n")


  # Then identify the flashers
  octopi.each_with_index do |octopi_row, row|
    octopi_row.each_with_index do |octopus, col|
      if octopus.flashable?
        # puts "Flasher at #{row}, #{col}! (Energy: #{octopus.energy})"
        octopus.flash!
        step_adjacent(octopi, row, col)
      end
    end
  end
  # Then count & reset the flashers
  flashers = 0
  octopi.each do |octopi_row|
    octopi_row.each do |octopus|
      if octopus.flashed
        flashers += 1
        octopus.reset!
      end
    end
  end
  flashers
end

# Given the starting energy levels of the dumbo octopuses in your cavern, simulate 100 steps. How many total flashes are there after 100 steps?

# Mini example
# input = [
#   [1,1,1,1,1],
#   [1,9,9,9,1],
#   [1,9,1,9,1],
#   [1,9,9,9,1],
#   [1,1,1,1,1]
# ]

# Test
# input = [
#   [5,4,8,3,1,4,3,2,2,3],
#   [2,7,4,5,8,5,4,7,1,1],
#   [5,2,6,4,5,5,6,1,7,3],
#   [6,1,4,1,3,3,6,1,4,6],
#   [6,3,5,7,3,8,5,4,7,8],
#   [4,1,6,7,5,2,4,6,4,5],
#   [2,1,7,6,8,4,1,7,2,1],
#   [6,8,8,2,8,8,1,1,3,4],
#   [4,8,4,6,8,4,8,5,5,4],
#   [5,2,8,3,7,5,1,5,2,6]
# ]

# Actual input
input = [
  [3,1,1,3,2,8,4,8,8,6],
  [2,8,5,1,8,7,6,1,4,4],
  [2,7,7,4,6,6,4,4,8,4],
  [6,7,1,5,1,1,2,5,7,8],
  [7,1,4,6,2,7,2,1,5,3],
  [6,2,5,6,6,5,6,3,6,7],
  [3,1,4,8,6,6,6,2,4,5],
  [3,8,5,7,4,4,6,5,2,8],
  [7,3,2,2,4,2,2,8,3,3],
  [8,1,5,2,1,7,5,1,6,8]
]


octopi = input.collect do |r|
  r.collect do |v|
    Octopus.new(energy: v)
  end
end

# puts octopi.map {|r| r.collect(&:energy).join('') }.join("\n")

flashes = 0
100_000.times do |i|
  flashes_this_step = step(octopi)

  # The logic below, breaking on first simultanious flash, is Puzzle 2
  flashes += flashes_this_step
  if flashes_this_step.eql?(100)
    puts "All flashed on step #{i + 1}"
    break
  end
  
end

# puts "After step 10:"
# puts octopi.map {|r| r.collect(&:energy).join('') }.join("\n")
# puts "#{flashes} flashes"

