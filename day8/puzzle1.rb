# For each display, you watch the changing signals for a while, make a note of all ten unique signal patterns you see, and then write down a single four digit output value (your puzzle input)
# 
# Using the signal patterns, you should be able to work out which pattern corresponds to which digit
#
# The goal for puzzle 1 is to identify the numbers 1 (2 segments), 4 (4 segments), 7 (3 segments), and 8 (7 segments)

# Each entry consists of ten unique signal patterns, a | delimiter, and finally the four digit output value.
entries = File.readlines("input.txt", chomp: true)

matches = 0
entries.each do |entry|
  patterns, outputs = entry.split("|")
  # Ignore the patterns for now
  # Look at the outputs and count how many match a 1, 4, 7, or 3
  matches += outputs.split(' ').select { |m| [2,4,3,7].include?(m.length) }.length
end

puts "#{matches} instances of digits that use a unique number of segments"

