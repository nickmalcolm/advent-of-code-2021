# For each display, you watch the changing signals for a while, make a note of all ten unique signal patterns you see, and then write down a single four digit output value (your puzzle input)
# 
# Using the signal patterns, you should be able to work out which pattern corresponds to which digit
#
# The goal for puzzle 1 is to identify the numbers 1 (2 segments), 4 (4 segments), 7 (3 segments), and 8 (7 segments)


# Each output uses a number of segments to display itself.
# For a given pattern, using its length, we can determine
# which numbers it could be


# Some helper methods

def char_for(pattern, char)
  pattern.include?(char) ? char : '.'
end

def number_printer(pattern)
  print " #{char_for(pattern, 'a') * 4} \n"
  print "#{char_for(pattern, 'b')}    #{char_for(pattern, 'c')}\n"
  print "#{char_for(pattern, 'b')}    #{char_for(pattern, 'c')}\n"
  print " #{char_for(pattern, 'd') * 4} \n"
  print "#{char_for(pattern, 'e')}    #{char_for(pattern, 'f')}\n"
  print "#{char_for(pattern, 'e')}    #{char_for(pattern, 'f')}\n"
  print " #{char_for(pattern, 'g') * 4} \n"
end

def translate(original, map)
  original.split('').collect {|p| map[p].first }.sort.join
end

def value_for(map, pattern)
  # This could be simplified based on pattern
  # length, but translating is a good bug catcher
  sorted_pattern = pattern.split('').sort.join
  case sorted_pattern
  when translate("abcefg", map)
    return 0
  when translate("cf", map)
    return 1
  when translate("acdeg", map)
    return 2
  when translate("acdfg", map)
    return 3
  when translate("bcdf", map)
    return 4
  when translate("abdfg", map)
    return 5
  when translate("abdefg", map)
    return 6
  when translate("acf", map)
    return 7
  when "abcdefg"
    return 8
  when translate("abcdfg", map)
    return 9
  else
    raise "Uh oh #{sorted_pattern}"
  end
end


# Each entry consists of ten unique signal patterns, a | delimiter, and finally the four digit output value.
# entry = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"

entries = File.readlines("input.txt", chomp: true)

total_value = 0

entries.each do |entry|
  patterns, outputs = entry.split("|").collect {|str| str.split(' ') }

  #  aaaa 
  # b    c
  # b    c
  #  dddd 
  # e    f
  # e    f
  #  gggg 
  map = {
    "a" => [],
    "b" => [],
    "c" => [],
    "d" => [],
    "e" => [],
    "f" => [],
    "g" => [],
  }
    
  # Figure out which could be 1
  one = patterns.select {|p| p.length == 2}.first.split('')
  map["c"] = one
  map["f"] = one

  # Figure out which could be 7
  seven = patterns.select {|p| p.length == 3}.first.split('')
  # Since seven and one have two segments in common,
  # the _uncommon_ value will be the mapping for segment a.
  # This is our first sure segment.
  map["a"] = seven - one

  # Figure out which could be 4
  four = patterns.select {|p| p.length == 4}.first.split('')
  # Four will introduce options for segments b and d
  options = four - seven - one
  map["b"] = options
  map["d"] = options

  # Figure out which could be 8
  eight = patterns.select {|p| p.length == 7}.first.split('')
  # Eight will introduce the remaining options
  options = eight - four - seven - one
  map["e"] = options
  map["g"] = options

  # puts "We've done 1, 7, 4 and 6"
  # puts map



  # The patterns with five are either
  # 2, 3, or 5.
  # Position e only occurs in 2, so
  # the letter(s) distinct in this group
  # will be that
  options = patterns.select {|p| p.length == 5}.join.split('')
  actual_options = options.select {|char| options.count(char) == 1 }
  # So now we have our initial options, and some new options.
  # Getting the intersection will provide our next sure value
  map["e"] = map["e"] & actual_options

  # We can go through our other candidates and remove it
  # Now we also know the position at g (somehow?... logic?...)
  map["g"] -= map["e"]

  # puts "We've done the 2 vs 3 vs 5 logic"
  # puts map



  # We can also identify the pattern which is two
  two = patterns.select {|p| p.length == 5}.select {|p| p.include?(map["e"].first)}.first.split('')
  # We know two, which includes signal c but not f.
  # We know one, which includes ONLY c and f.
  # Whichever value from one is also in two is therefore c.
  map["c"] = one & two
  # And because reasons?...
  map["f"] -= map["c"]

  # puts "We know a c e f and g"
  # puts map

  # We know two, and all the values for two except signal d
  map["d"] = two - map["a"] - map["c"] - map["e"] - map["g"]
  # And because reasons?...
  map["b"] -= map["d"]

  # puts "We know a b c d e f and g!"
  # puts map



  # patterns.each do |pattern|
  #   puts "#{pattern}: #{value_for(map, pattern)}"
  # end

  value = outputs.collect do |pattern|
    value_for(map, pattern)
  end.join.to_i
  total_value += value
  # puts "#{outputs.join(' ')}: #{value}"
end

puts "Total: #{total_value}"